from random import randint

LoopCounter = 0
NumRepeat = randint(1, 20)

print "Num. Repeat:\t%s" % NumRepeat

for LoopCounter in range(0,NumRepeat):
	DeleteList = []
	UsedList = [19,14,18,16,5,7,17,10,4,9,2,11,8]

	Spacer  = ""
	Spacer += "*" * 40 + " "
	Spacer += str(LoopCounter + 1)
	Spacer += " " + "*" * 40

	print Spacer

	for i in range(1, 20 - len(UsedList)):
		while True:
			R = randint(1, 20)

			if R in UsedList:
				continue

			if R not in DeleteList:
				DeleteList.append(R)
				break

	print "Deleted List:\t%s" % DeleteList
	print "Used List:\t%s" % UsedList

	for R in range(1, 21):
		if R in UsedList:
			continue

		if R not in DeleteList:
			print "\t\tHappy Group ID *%s* :))" % R

	Spacer  = ""
	Spacer += "*" * 85 + " "
	print Spacer






	
